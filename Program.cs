﻿using System;
using System.Data.Common;
using System.Diagnostics;

namespace gameme
{
    class Program
    {
        static int[,] pole, visible;
        static Random rand;
        static void Main(string[] args)
        {
            pole = new int[10, 10];
            visible = new int[10, 10];
            int health = 10;
            int px = 0, py = 0;
            rand = new Random();
            ConsoleKey button;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    pole[i, j] = 0;
                    visible[i, j] = 0;
                }
            } //поле = 0
            pole[0, 0] = 1;//start
            pole[9, 9] = 1;//finish
            for (int i = 0; i < 10; i++)
            {
                SetBomb();
            } // расстановка бомб
            #region рисовка
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    visible[j, i] = 0;
                }
            }
            visible[0, 0] = 1;
            #endregion
            asd(health);
            try
            {
                while (true)
                {
                    button = Console.ReadKey().Key;
                    Console.Clear();
                    if (button == ConsoleKey.W)
                    {
                        visible[px, py] = 0;
                        px--;
                        if (px < 0)
                        {
                            px++;
                        }
                        visible[px, py] = 1;
                    }
                    if (button == ConsoleKey.S)
                    {
                        visible[px, py] = 0;
                        px++;
                        if (px > 9)
                        {
                            px--;
                        }
                        visible[px, py] = 1;
                    }
                    if (button == ConsoleKey.D)
                    {
                        visible[px, py] = 0;
                        py++;
                        if (py > 9)
                        {
                            py--;
                        }
                        visible[px, py] = 1;
                    }
                    if (button == ConsoleKey.A)
                    {
                        visible[px, py] = 0;
                        py--;
                        if (py < 0)
                        {
                            py++;
                        }
                        visible[px, py] = 1;
                    }
                    Check(px, py, ref health);
                    asd(health);
                }
            }
            catch 
            { 
                Lose(); 
            }
        }
        static void SetBomb()
        {
            int x, y;
            x = rand.Next(0, 10);
            y = rand.Next(0, 10);
            if (pole[x, y] == 0)
            {
                pole[x, y] = -1;
            }
            else
            {
                SetBomb();
            }
        }
        static void asd(int health)
        {
            Console.WriteLine();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (visible[i,j] == 1)
                    {
                        Console.Write(" 0");
                    }
                    else
                    {
                        Console.Write(" *");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine("Health: " + health);
        } //вывод поля
        static void Lose()
        {
            Console.Clear();
            for (int i = 0; i < 12; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < 50; j++)
                {
                    Console.Write(" ");
                }
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("You lose!");
            for (int j = 0; j < 50; j++)
            {
                Console.Write(" ");
            }
            Console.WriteLine("Что бы повторить нажмите 'Y'");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Process.Start("gameme");
                Environment.Exit(0);
            }
            else
            {
                Environment.Exit(0);
            }
        }
        static void Win()
        {
            Console.Clear();
            for (int i = 0; i < 12; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < 50; j++)
                {
                    Console.Write(" ");
                }
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("You win!");
            for (int j = 0; j < 50; j++)
            {
                Console.Write(" ");
            }
            Console.WriteLine("Что бы повторить нажмите 'Y'");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Process.Start("gameme");
                Environment.Exit(0);
            }
            else
            {
                Environment.Exit(0);
            }
        }
        static void Check(int x, int y, ref int health)
        {
            int damage;
            if (x == 9 && y == 9)
            {
                Win();
            }
            if (pole[x,y] == -1)
            {
                damage = rand.Next(10) + 1;
                health -= damage;
                pole[x, y] = 0;
                if (health <= 0)
                {
                    Lose();
                }
            }
        }
    }
}
